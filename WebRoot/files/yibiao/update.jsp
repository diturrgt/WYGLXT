<%@ page language="java" import="java.util.*,java.sql.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="pg"  uri="http://jsptags.com/tags/navigation/pager" %>
<%@ taglib prefix="fmt"  uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!-- 
功能介绍：修改

 -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<script language="javascript" type="text/javascript" src="<%=request.getContextPath()%>/My97DatePicker/WdatePicker.js"></script>
<link rel="stylesheet" rev="stylesheet" href="css/style.css" type="text/css" media="all" />
<style type="text/css">
<!--
.atten {font-size:12px;font-weight:normal;color:#F00;}
-->
</style>
<script type="text/javascript">
function checkValue(){
	if(document.mainForm.bianhao.value==""||document.mainForm.bianhao.value==null)
	{
		alert("不能为空！");
		document.mainForm.bianhao.focus();
		return false;
	}
	if(document.mainForm.zhuhu.value==""||document.mainForm.zhuhu.value==null)
	{
		alert("不能为空！");
		document.mainForm.zhuhu.focus();
		return false;
	}
	if(document.mainForm.startTime.value==""||document.mainForm.startTime.value==null)
	{
		alert("不能为空！");
		document.mainForm.startTime.focus();
		return false;
	}
	if(document.mainForm.endTime.value==""||document.mainForm.endTime.value==null)
	{
		alert("不能为空！");
		document.mainForm.endTime.focus();
		return false;
	}
	if(document.mainForm.feiyong.value==""||document.mainForm.feiyong.value==null)
	{
		alert("不能为空！");
		document.mainForm.feiyong.focus();
		return false;
	}
	if(document.mainForm.shuzhi.value==""||document.mainForm.shuzhi.value==null)
	{
		alert("不能为空！");
		document.mainForm.shuzhi.focus();
		return false;
	}
	return true;
}
</script>
</head>

<body class="ContentBody">
  <form action="yibiao_updateYibiao" method="post"  name="mainForm" id="mainForm" onSubmit="return checkValue()" >
<div class="MainDiv">
<table width="99%" border="0" cellpadding="0" cellspacing="0" class="CContent">
   <tr>
      <td height="10">&nbsp;</td>
  </tr>
  <tr>
      <td align="left" ><font color="blue">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;修改</font></td>
  </tr>
  <tr>
    <td class="CPanel">		
		<table border="0" cellpadding="0" cellspacing="0" style="width:100%">		
		<tr>
			<td width="100%">
				<fieldset style="height:100%;">
				<legend>停车信息</legend>
				 <input name='id' type="hidden" class="text" style="width:354px" value="<s:property value="#request.yibiao.id"/>"/>
					  <table border="0" cellpadding="2" cellspacing="1" style="width:100%">
					  <tr>
					    <td align="right" width="19%">仪表编号:</td>
					    <td width="35%"><span class="red">
				        <input name='bianhao' type="text" class="text" style="width:354px" value="<s:property value="#request.yibiao.bianhao"/>"/>
				        *</span>
				        </td>
					  </tr>
					  <tr>
					    <td align="right" width="19%">住户:</td>
					    <td width="35%"><span class="red">
				        <input name='zhuhu' type="text" class="text" style="width:354px" value="<s:property value="#request.yibiao.zhuhu"/>"/>
				        *</span>
				        </td>
					  </tr>
					  <tr>
					    <td align="right" width="19%">开始时间:</td>
					    <td width="35%"><span class="red">
				        <input name='startTime' type="text" class="Wdate"  onfocus="WdatePicker({readOnly:true,dateFmt:'yyyy-MM-dd HH:mm:ss'})" style="width:354px" value="<s:property value="#request.yibiao.startTime"/>"/>
				        *</span>
				        </td>
					  </tr>
					  <tr>
					    <td align="right" width="19%">结束时间:</td>
					    <td width="35%"><span class="red">
					    <input name='endTime' type="text" class="Wdate"  onfocus="WdatePicker({readOnly:true,dateFmt:'yyyy-MM-dd HH:mm:ss'})" style="width:354px" value="<s:property value="#request.yibiao.endTime"/>"/>
				        *</span>
				        </td>
					  </tr>
					  <tr>
					    <td align="right" width="19%">费用:</td>
					    <td width="35%"><span class="red">
				        <input name='feiyong' type="text" class="text" style="width:354px" value="<s:property value="#request.yibiao.feiyong"/>"/>
				        *</span>
				        </td>
					  </tr>
					  <tr>
					    <td align="right" width="19%">数值:</td>
					    <td width="35%"><span class="red">
				        <input name='shuzhi' type="text" class="text" style="width:354px" value="<s:property value="#request.yibiao.shuzhi"/>"/>
				        *</span>
				        </td>
					  </tr>
					  </table>
			  <br />
				</fieldset>			
			</td>
		</tr>
		</table>
	 </td>
  </tr>
	<tr>
		<td colspan="2" align="center" height="50px">
		<input type="submit" name="submitbut" value="保存" class="button" />　
		
		<input type="reset" name="reset" value="重置" class="button"  />
		</td>
	</tr>
</table>
</div>
</form>
</body>
</html>
