<%@ page language="java" import="java.util.*,java.sql.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="pg"  uri="http://jsptags.com/tags/navigation/pager" %>
<%@ taglib prefix="fmt"  uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!-- 
功能介绍：修改

 -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<link rel="stylesheet" rev="stylesheet" href="css/style.css" type="text/css" media="all" />
<style type="text/css">
<!--
.atten {font-size:12px;font-weight:normal;color:#F00;}
-->
</style>
<script type="text/javascript">
		function checkValue(){
				if(document.mainForm.password.value==""||document.mainForm.password.value==null)
				{
					alert("不能为空！");
					document.mainForm.password.focus();
					return false;
				}
				if(document.mainForm.realname.value==""||document.mainForm.realname.value==null)
				{
					alert("不能为空！");
					document.mainForm.realname.focus();
					return false;
				}
				return true;
		}
</script>
</head>

<body class="ContentBody">
  <form action="user_updateMy" method="post"  name="mainForm" id="mainForm" onSubmit="return checkValue()" >
<div class="MainDiv">
<table width="99%" border="0" cellpadding="0" cellspacing="0" class="CContent">
   <tr>
      <td height="10">&nbsp;</td>
  </tr>
  <tr>
      <td align="left" ><font color="blue">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;个人信息</font></td>
  </tr>
  <tr>
    <td class="CPanel">		
		<table border="0" cellpadding="0" cellspacing="0" style="width:100%">		
		<tr>
			<td width="100%">
				<fieldset style="height:100%;">
				<legend>个人信息</legend>
					  <table border="0" cellpadding="2" cellspacing="1" style="width:100%">
					  <tr>
					    <td align="right" width="19%">账号:</td>
					    <td width="35%"><span class="red">
				        <s:property value="#request.user.username"/>
				        </span>
				        </td>
					  </tr>
					  <tr>
					    <td align="right" width="19%">姓名:</td>
					    <td width="35%"><span class="red">
				        <s:property value="#request.user.realname"/></span>
				        </td>
					  </tr>
					  <tr>
					    <td align="right" width="19%">性别:</td>
					    <td width="35%"><span class="red">
					    <s:property value="#request.user.sex"/>
					    </span>
				        </td>
					  </tr>
					  <tr>
					    <td align="right" width="19%">电话:</td>
					    <td width="35%"><span class="red"><s:property value="#request.user.tel"/></span>
				        </td>
					  </tr>
					  <tr>
					    <td align="right" width="19%">住户地址:</td>
					    <td width="35%"><span class="red"><s:property value="#request.user.className"/></span>
				        </td>
					  </tr>
					  <tr>
					    <td align="right" width="19%">身份证:</td>
					    <td width="35%"><span class="red"><s:property value="#request.user.major"/></span>
				        </td>
					  </tr>
					  <tr>
					    <td align="right" width="19%">邮件:</td>
					    <td width="35%"><span class="red"><s:property value="#request.user.email"/></span>
				        </td>
					  </tr>
					  <tr>
					    <td align="right" width="19%">QQ:</td>
					    <td width="35%"><span class="red"><s:property value="#request.user.qq"/></span>
				        </td>
					  </tr>
					  </table>
			  <br />
				</fieldset>			
			</td>
		</tr>
		</table>
	 </td>
  </tr>
	<tr>
		<td colspan="2" align="center" height="50px">
		</td>
	</tr>
</table>
</div>
</form>
</body>
</html>
