<%@ page language="java" import="java.util.*,java.sql.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="pg"  uri="http://jsptags.com/tags/navigation/pager" %>
<%@ taglib prefix="fmt"  uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!-- 
功能介绍：修改

 -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<script language="javascript" type="text/javascript" src="<%=request.getContextPath()%>/My97DatePicker/WdatePicker.js"></script>
<link rel="stylesheet" rev="stylesheet" href="css/style.css" type="text/css" media="all" />
<style type="text/css">
<!--
.atten {font-size:12px;font-weight:normal;color:#F00;}
-->
</style>
<script type="text/javascript">
function checkValue(){
	if(document.mainForm.title.value==""||document.mainForm.title.value==null)
	{
		alert("不能为空！");
		document.mainForm.title.focus();
		return false;
	}
	if(document.mainForm.tel.value==""||document.mainForm.tel.value==null)
	{
		alert("不能为空！");
		document.mainForm.tel.focus();
		return false;
	}
	if(document.mainForm.dizi.value==""||document.mainForm.dizi.value==null)
	{
		alert("不能为空！");
		document.mainForm.dizi.focus();
		return false;
	}
	if(document.mainForm.remark.value==""||document.mainForm.remark.value==null)
	{
		alert("不能为空！");
		document.mainForm.remark.focus();
		return false;
	}
	return true;
}
</script>
</head>

<body class="ContentBody">
  <form action="baoxiu_updateBaoxiu" method="post"  name="mainForm" id="mainForm" onSubmit="return checkValue()" >
<div class="MainDiv">
<table width="99%" border="0" cellpadding="0" cellspacing="0" class="CContent">
   <tr>
      <td height="10">&nbsp;</td>
  </tr>
  <tr>
      <td align="left" ><font color="blue">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;添加</font></td>
  </tr>
  <tr>
    <td class="CPanel">		
		<table border="0" cellpadding="0" cellspacing="0" style="width:100%">		
		<tr>
			<td width="100%">
				<fieldset style="height:100%;">
				<legend>报修信息</legend>
				 <input name='id' type="hidden" class="text" style="width:354px" value="<s:property value="#request.baoxiu.id"/>"/>
					  <table border="0" cellpadding="2" cellspacing="1" style="width:100%">
					  <tr>
					    <td align="right" width="19%">报修问题:</td>
					    <td width="35%"><span class="red">
				        <input name='title' type="text" class="text" style="width:354px" value="<s:property value="#request.baoxiu.title"/>"/>
				        *</span>
				        </td>
					  </tr>
					  <tr>
					    <td align="right" width="19%">电话:</td>
					    <td width="35%"><span class="red">
				        <input name='tel' type="text" class="text" style="width:354px" value="<s:property value="#request.baoxiu.tel"/>"/>
				        *</span>
				        </td>
					  </tr>
					  <tr>
					    <td align="right" width="19%">地址:</td>
					    <td width="35%"><span class="red">
				        <input name='dizi' type="text" class="text" style="width:354px" value="<s:property value="#request.baoxiu.dizi"/>"/>
				        *</span>
				        </td>
					  </tr>
					  <tr>
					    <td align="right" width="19%">备注:</td>
					    <td width="35%"><span class="red">
					    <textarea name="remark" rows="4" cols="40"><s:property value="#request.baoxiu.remark"/></textarea>
				        *</span>
				        </td>
					  </tr>
					  </table>
			  <br />
				</fieldset>			
			</td>
		</tr>
		</table>
	 </td>
  </tr>
	<tr>
		<td colspan="2" align="center" height="50px">
		<input type="submit" name="submitbut" value="保存" class="button" />　
		
		<input type="reset" name="reset" value="重置" class="button"  />
		</td>
	</tr>
</table>
</div>
</form>
</body>
</html>
