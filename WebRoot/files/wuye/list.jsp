<%@ page language="java" import="java.util.*,java.sql.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="pg"  uri="http://jsptags.com/tags/navigation/pager" %>
<%@ taglib prefix="fmt"  uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!-- 
功能介绍：管理员信息

 -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
.tabfont01 {	
	font-family: "宋体";
	font-size: 9px;
	color: #555555;
	text-decoration: none;
	text-align: center;
}
.font051 {font-family: "宋体";
	font-size: 12px;
	color: #333333;
	text-decoration: none;
	line-height: 20px;
}
.font201 {font-family: "宋体";
	font-size: 12px;
	color: #FF0000;
	text-decoration: none;
}
.button {
	font-family: "宋体";
	font-size: 14px;
	height: 37px;
}
html { overflow-x: auto; overflow-y: auto; border:0;} 
-->
</style>

<link href="css/css.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<form name="mainForm" id="mainForm" method="post" action="wuye_queryWuye">
<table width="100%" border="0" cellspacing="0" cellpadding="0">

  <tr>
    <td height="30"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td height="62" background="images/nav04.gif">
          
		   <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
		  <tr>
			<td width="21"><img src="images/ico07.gif" width="20" height="18" /></td>
			<td >查看内容： 按编号：<input type="text" name="searchname" value="<s:property value="#request.searchname"/>" />           
              <input name="Submit" type="submit" class="right-button02" value="查 询" /></td>
		  </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table id="subtree1" style="DISPLAY: " width="100%" border="0" cellspacing="0" cellpadding="0">

        <tr>
          <td><table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">

          	 <tr>
               <td height="20"><font color="blue"  >物业管理</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="/classweb/files/wuye/add.jsp"><font color="red">>>添加<<</font></a></td>
          	 </tr>
          	 <tr>
               <td height="10" valign="top" ><hr size="1" color="green" /></td>
          	 </tr>
              <tr>
                <td height="40" class="font42">
                <table width="100%" border="0" cellpadding="4" cellspacing="1" bgcolor="#464646" >
                  <tr>
                    <td  align="center" bgcolor="#EEEEEE">序列</td>
                    <td  align="center" bgcolor="#EEEEEE">业务内容</td>
				    <td  align="center" bgcolor="#EEEEEE">物业编号</td>
				    <td  align="center" bgcolor="#EEEEEE">住户</td>
				    <td  align="center" bgcolor="#EEEEEE">开始时间</td>
				    <td  align="center" bgcolor="#EEEEEE">结束时间</td>
				    <td  align="center" bgcolor="#EEEEEE">费用</td>
                    <td  align="center" bgcolor="#EEEEEE">操作</td>
                  </tr>
                  <s:iterator value="#request.pageinfo.datas" var="list" status="i">
	                  <tr>
	                    <td bgcolor="#FFFFFF" width="5%" height="20" align="center" ><s:property value="#i.index+1"/></td>
	                    <td bgcolor="#FFFFFF"  align="center"><s:property value="#list.title"/></td>
					    <td bgcolor="#FFFFFF"  align="center"><s:property value="#list.bianhao"/></td>
					    <td bgcolor="#FFFFFF"  align="center"><s:property value="#list.zhuhu"/></td>
					    <td bgcolor="#FFFFFF"  align="center"><s:property value="#list.startTime"/></td>
					    <td bgcolor="#FFFFFF"  align="center"><s:property value="#list.endTime"/></td>
					    <td bgcolor="#FFFFFF"  align="center"><s:property value="#list.feiyong"/></td>
	                    <td bgcolor="#FFFFFF"  align="center"><a href="wuye_preupdateWuye?id=<s:property value="#list.id"/>">修改</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="wuye_delWuye?id=<s:property value="#list.id"/>">删除</a></td>
	                  </tr>
	                </s:iterator>
                </table></td>
              </tr>
              <tr>
              	<td>
              	<jsp:include flush="true" page="../../pagetag.jsp"></jsp:include>
              	</td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
</table>
</form>
</body>
</html>
