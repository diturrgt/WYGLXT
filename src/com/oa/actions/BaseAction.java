package com.oa.actions;
import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import org.apache.commons.lang.xwork.StringUtils;
import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class BaseAction extends ActionSupport {
	//分页变量
	public String currentpage = "1";
	public String pageunit = "10";
	

	public String getCurrentpage() {
		return currentpage;
	}

	public void setCurrentpage(String currentpage) {
		this.currentpage = currentpage;
	}

	public String getPageunit() {
		return pageunit;
	}

	public void setPageunit(String pageunit) {
		this.pageunit = pageunit;
	}
	public String getCurrentpage(HttpServletRequest request) {
		String currentpage = request.getParameter("currentpage");
		if(currentpage != null && !"".equals(currentpage)){
			return currentpage;
		}
		return this.currentpage;
	}

	public String getPageunit(HttpServletRequest request,String str) {
		String pageunit= request.getParameter("pageunit");
		if(pageunit != null && !"".equals(pageunit)){
			request.getSession().setAttribute(str, pageunit);
			return pageunit;
		}else{
			return request.getSession().getAttribute(str).toString();
		}
	}

	/**
	 * @Description: 获取 原生Request对象
	 * @return HttpServletRequest
	 */
	public HttpServletRequest getHttpServletRequest() {

		return ServletActionContext.getRequest();

	}

	/**
	 * @Description: 获取 在 Strus2 <b>HttpServletRequest</b> 封装 的 <b>requestMap</b>
	 * @return Map<String,Object>
	 */
	public Map<String, Object> getRequestMap() {
		ActionContext actionContext = ActionContext.getContext();
		Map<String, Object> requestMap = actionContext.getContextMap();
		return requestMap;
	}

	/**
	 * @Description: 取得 Request 的 Attribute
	 * @param name
	 * @return Object
	 */
	public Object getRequestAttribute(String name) {
		return getRequestMap().get(name);
	}

	/**
	 * @Description: 设置Request Attribute
	 * @param name
	 * @param value
	 * @return void
	 */
	public void setRequestAttribute(String name, Object value) {

		getRequestMap().put(name, value);
	}

	/**
	 * @Description: 获取Requst 的 Parameter 有值 返回， 否者 返回
	 * @param name
	 * @return String
	 */
	public String getRequestParameter(String name) {

		if (getRequestParameterMap().get(name)!=null) {
			return getHttpServletRequest().getParameter(name);
		} else {
			return StringUtils.EMPTY;
		}
	}

	/**
	 * @Description: 获取所有请求参数
	 * @return Map<String,Object>
	 */
	public Map<String, Object> getRequestParameterMap() {
		ActionContext actionContext = ActionContext.getContext();
		Map<String, Object> parameterMap = actionContext.getParameters();
		return parameterMap;
	}

	/**
	 * 
	 * 
	 * @Description: 获取请求参数 ， 值的数组
	 * 
	 * @param name
	 * @return String[]
	 */
	public String[] getParameterValues(String name) {
		return getHttpServletRequest().getParameterValues(name);
	}

	/**
	 * @Description: 获取原生的 HttpSession
	 * @return HttpSession
	 */
	public HttpSession getHttpSession() {
		return getHttpServletRequest().getSession();
	}

	/**
	 * @Description: 获取 被Strus2 封装<b>HttpSession</b> <b>sessionMap</b>
	 * @return Map<String,Object>
	 */
	public Map<String, Object> getSessionMap() {

		ActionContext actionContext = ActionContext.getContext();
		Map<String, Object> sessionMap = actionContext.getSession();
		return sessionMap;
	}

	/**
	 * @Description: 获取 sessionMap 中的 Attribute
	 * @param name
	 * @return Object
	 */
	public Object getSessionAttribute(String name) {
		return getSessionMap().get(name);
	}

	/**
	 * 
	 * 
	 * @Description: 设置 Attribute 到 sessionMap中
	 * 
	 * @param name
	 * @param value
	 * @return void
	 * @throws
	 */
	public void setSessionAttribute(String name, Object value) {
		getSessionMap().put(name, value);
	}

	/**
	 * @Description: 获取 原生的 HttpServletResponse
	 * 
	 * @return HttpServletResponse
	 */
	public HttpServletResponse getHttpServletResponse() {
		return ServletActionContext.getResponse();
	}

	/**
	 * @Description: 获取 ServletContext 对象
	 * 
	 * @return ServletContext
	 */
	public ServletContext getApplication() {
		return ServletActionContext.getServletContext();
	}

	/**
	 * 
	 * 
	 * @Description: AJAX输出
	 * 
	 * @param content
	 * @param type
	 * @return String
	 * @throws
	 */
	public String ajax(String content, String type) {
		try {
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType(type + ";charset=UTF-8");
			response.setHeader("Pragma", "No-cache");
			response.setHeader("Cache-Control", "no-cache");
			response.setDateHeader("Expires", 0);
			response.getWriter().write(content);
			response.getWriter().flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * @Description: AJAX输出 <b>文本</b> 格式:<b>text/plain</b>
	 * @param text 文本
	 * @return String
	 */
	public String ajaxText(String text) {
		return ajax(text, "text/plain");
	}

	/**
	 * @Description: AJAX输出 <b>html</b> 格式:<b>text/html</b>
	 * @param html
	 * @return String
	 */
	public String ajaxHtml(String html) {
		return ajax(html, "text/html");
	}

	/**
	 * @Description: AJAX输出 <b>xml</b> 格式: <b>text/xml</b>
	 * @param xml数据--->客户端
	 * @return String
	 */
	public String ajaxXml(String xml) {
		return ajax(xml, "text/xml");
	}

	/**
	 * @Description: AJAX输出 <b>json</b> 串 格式:<b> text/html</b>
	 * @param 直接输出
	 *            Json--->客户端
	 * @return String
	 */
	public String ajaxJson(String jsonString) {
		return ajax(jsonString, "text/html");
	}




	/**
	 * 
	 * @Description: 设置页面不缓存
	 * 
	 * @return void
	 * @throws
	 */
	public void setResponseNoCache() {
		getHttpServletResponse().setHeader("progma", "no-cache");
		getHttpServletResponse().setHeader("Cache-Control", "no-cache");
		getHttpServletResponse().setHeader("Cache-Control", "no-store");
		getHttpServletResponse().setDateHeader("Expires", 0);
	}



	private static final long serialVersionUID = 4711141009520101116L;
	public static final String VIEW = "view";
	public static final String LIST = "list";
	public static final String STATUS = "status";
	public static final String WARN = "warn";
	public static final String SUCCESS = "success";
	public static final String ERROR = "error";
	public static final String MESSAGE = "message";
	
	public static final String SAVESUCCESS = "添加成功";
	public static final String SAVEFAILE = "添加失败";
	public static final String UPDATESUCCESS = "修改成功";
	public static final String UPDATEFAILE = "修改失败";
	public static final String DELETESUCCESS = "删除成功";
	public static final String DELETEFAILE = "删除失败";
	public static final String EXISTS = "已存在";
	public static final String MOVEFAILE = "移动失败";

	protected String id;
	protected String[] ids;

}

