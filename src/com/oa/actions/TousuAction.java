package com.oa.actions;
import org.apache.struts2.ServletActionContext;

import com.oa.models.TTousu;
import com.oa.models.TUser;
import com.oa.services.TousuServices;
import com.oa.utils.PageInfo;

/**
 * 
 * 
 * 投诉
 *
 */
public class TousuAction extends BaseAction{
	private TousuServices tousuServices;
	private Integer id;
	private String num;
	private String remark;
	private String sizenum;
	private String searchname;
	/**
	 * 
	 *
	 * 分页
	 *
	 */
	public String queryTousu() throws Exception{
		if (getSessionAttribute("querypageunit") == null) {
			setSessionAttribute("querypageunit",this.pageunit);
		}
		
		StringBuffer cond = new StringBuffer();
		if(null!=searchname&&""!=searchname.trim()){
			cond.append(" and a.num like '%"+searchname.trim()+"%' ");
		}
		if(null!=getSessionAttribute("user")&&"1".equals(((TUser)getSessionAttribute("user")).getPower())){
			cond.append(" and a.sizenum = '"+((TUser)getSessionAttribute("user")).getRealname()+"' ");
		}
		int curpage = Integer.parseInt(this.getCurrentpage(ServletActionContext.getRequest()));
		int pageunit = Integer.parseInt(this.getPageunit(ServletActionContext.getRequest(), "querypageunit"));

		String url = "tousu_queryTousu?a=a";
		
		PageInfo pageInfo = this.tousuServices.queryTousu(curpage,
				pageunit, ServletActionContext.getRequest(), url, cond.toString());
		setRequestAttribute("pageinfo", pageInfo);
		return "queryTousu";
	}
	/**
	 * 
	 *
	 * 添加
	 *
	 */
	public String addTousu(){
		try {
			TTousu ttousu = new TTousu();
			ttousu.setNum(num);
			ttousu.setRemark(remark);
			ttousu.setSizenum(((TUser)getSessionAttribute("user")).getRealname());
			tousuServices.addTousu(ttousu);
		} catch (RuntimeException e) {
			e.printStackTrace();
		}
		return "addTousu";
	}
	/**
	 * 
	 *
	 * 预添加
	 *
	 */
	public String preaddTousu() throws Exception{
		return "preaddTousu";
	}
	/**
	 * 
	 *
	 * 删除
	 *
	 */
	public String delTousu() throws Exception{
		tousuServices.delTousu(id);
		return "delTousu";
	}
	
	/**
	 * 
	 *
	 * 预修改
	 *
	 */
	public String preupdateTousu() throws Exception{
		TTousu tousu = this.getTousuServices().getTousu(id);
		setRequestAttribute("tousu",tousu);
		return "preupdateTousu";
	}
	 public String showTousu() throws Exception{
			TTousu tousu = this.getTousuServices().getTousu(id);
			setRequestAttribute("tousu",tousu);
			return "showTousu";
	}
	/**
	 * 
	 *
	 * 功能：修改
	 *
	 */
	public String updateTousu() {
		try {
			TTousu ttousu = this.getTousuServices().getTousu(id);
			ttousu.setNum(num);
			ttousu.setRemark(remark);
			this.getTousuServices().updateTousu(ttousu);
		} catch (RuntimeException e) {
			e.printStackTrace();
		}
		return "updateTousu";
	}
	
	public TousuServices getTousuServices() {
		return tousuServices;
	}
	public void setTousuServices(TousuServices tousuServices) {
		this.tousuServices = tousuServices;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNum() {
		return num;
	}
	public void setNum(String num) {
		this.num = num;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getSizenum() {
		return sizenum;
	}
	public void setSizenum(String sizenum) {
		this.sizenum = sizenum;
	}
	public String getSearchname() {
		return searchname;
	}
	public void setSearchname(String searchname) {
		this.searchname = searchname;
	}
	
}
