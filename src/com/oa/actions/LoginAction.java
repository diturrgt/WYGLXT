package com.oa.actions;


import org.apache.commons.lang.xwork.StringUtils;

import com.oa.models.TUser;
import com.oa.services.LoginServices;


public class LoginAction extends BaseAction{
	private LoginServices loginServices;
	
	private Integer id;
	private String username;
	private String password;
	private String realname;
	private String power;
	public String checkUser() throws Exception{
		if (!(StringUtils.isEmpty(username)|| StringUtils.isEmpty(password))) {
			TUser user = loginServices.checkUser(username,password,power);
			if (user!=null) {
				
				setSessionAttribute("user", user);
				return SUCCESS;
			}
			setRequestAttribute("error", "error");
		}
		return ERROR;
	}
	public String updateUser() throws Exception{
		TUser user = new TUser();
		user.setId(id);
		user.setPassword(password);
		user.setUsername(username);
		user.setRealname(realname);
		loginServices.updateUser(user);
		setSessionAttribute("user", user);
		return "update";
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getRealname() {
		return realname;
	}
	public void setRealname(String realname) {
		this.realname = realname;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public LoginServices getLoginServices() {
		return loginServices;
	}
	public void setLoginServices(LoginServices loginServices) {
		this.loginServices = loginServices;
	}
	public String getPower() {
		return power;
	}
	public void setPower(String power) {
		this.power = power;
	}
}
