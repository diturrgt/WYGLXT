package com.oa.actions;
import org.apache.struts2.ServletActionContext;

import com.oa.models.TWuye;
import com.oa.services.WuyeServices;
import com.oa.utils.PageInfo;

/**
 * 
 */
public class WuyeAction extends BaseAction{
	private WuyeServices wuyeServices;
	private Integer id;
	private String zhuhu;
	private String bianhao;
	private String startTime;
	private String endTime;
	private String feiyong;
	private String title;
	private String searchname;
	/**
	 * 
	 *
	 * 分页
	 *
	 */
	public String queryWuye() throws Exception{
		if (getSessionAttribute("querypageunit") == null) {
			setSessionAttribute("querypageunit",this.pageunit);
		}
		StringBuffer cond = new StringBuffer();
		int curpage = Integer.parseInt(this.getCurrentpage(ServletActionContext.getRequest()));
		int pageunit = Integer.parseInt(this.getPageunit(ServletActionContext.getRequest(), "querypageunit"));

		String url = "wuye_queryWuye?a=a";
		if(null!=searchname&&""!=searchname.trim()){
			cond.append(" and a.bianhao like '%"+searchname.trim()+"%' ");
		}
		PageInfo pageInfo = this.wuyeServices.queryWuye(curpage,
				pageunit, ServletActionContext.getRequest(), url, cond.toString());
		setRequestAttribute("pageinfo", pageInfo);
		return "queryWuye";
	}
	/**
	 * 
	 *
	 * 添加
	 *
	 */
	public String addWuye(){
		try {
			/*SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String strTime = format.format(new Date());*/
			TWuye twuye = new TWuye();
			twuye.setBianhao(bianhao);
			twuye.setTitle(title);
			twuye.setEndTime(endTime);
			twuye.setFeiyong(feiyong);
			twuye.setStartTime(startTime);
			twuye.setZhuhu(zhuhu);
			wuyeServices.addWuye(twuye);
		} catch (RuntimeException e) {
			e.printStackTrace();
		}
		return "addWuye";
	}
	/**
	 * 
	 *
	 * 预添加
	 *
	 */
	public String preupdateWuye() throws Exception{
		TWuye wuye =  wuyeServices.getWuye(id);
		setRequestAttribute("wuye", wuye);
		return "preupdateWuye";
	}
	/**
	 * 
	 *
	 * 功能：修改
	 *
	 */
	public String updateWuye() {
		try {
			TWuye twuye =  wuyeServices.getWuye(id);
			twuye.setBianhao(bianhao);
			twuye.setTitle(title);
			twuye.setEndTime(endTime);
			twuye.setFeiyong(feiyong);
			twuye.setStartTime(startTime);
			twuye.setZhuhu(zhuhu);
			wuyeServices.updateWuye(twuye);
		} catch (RuntimeException e) {
			e.printStackTrace();
		}
		return "updateWuye";
	}
	/**
	 * 
	 *
	 * 删除
	 *
	 */
	public String delWuye() throws Exception{
		wuyeServices.delWuye(id);
		return "delWuye";
	}
	public WuyeServices getWuyeServices() {
		return wuyeServices;
	}
	public void setWuyeServices(WuyeServices wuyeServices) {
		this.wuyeServices = wuyeServices;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getZhuhu() {
		return zhuhu;
	}
	public void setZhuhu(String zhuhu) {
		this.zhuhu = zhuhu;
	}
	public String getBianhao() {
		return bianhao;
	}
	public void setBianhao(String bianhao) {
		this.bianhao = bianhao;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public String getFeiyong() {
		return feiyong;
	}
	public void setFeiyong(String feiyong) {
		this.feiyong = feiyong;
	}
	public String getSearchname() {
		return searchname;
	}
	public void setSearchname(String searchname) {
		this.searchname = searchname;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
}
