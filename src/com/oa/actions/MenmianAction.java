package com.oa.actions;
import org.apache.struts2.ServletActionContext;

import com.oa.models.TMenmian;
import com.oa.services.MenmianServices;
import com.oa.utils.PageInfo;

/**
 * 
 */
public class MenmianAction extends BaseAction{
	private MenmianServices menmianServices;
	private String courseName;
	private String remark;
	private String dizi;
	private String zujin;
	private String mianji;
	private String tel;
	private String username;
	private String searchname;
	private String id;
	/**
	 * 
	 *
	 * 分页
	 *
	 */
	public String queryMenmian() throws Exception{
		if (getSessionAttribute("querypageunit") == null) {
			setSessionAttribute("querypageunit",this.pageunit);
		}
		StringBuffer cond = new StringBuffer();
		if(null!=searchname&&""!=searchname.trim()){
			cond.append(" and a.courseName like '%"+searchname.trim()+"%' ");
		}
		int curpage = Integer.parseInt(this.getCurrentpage(ServletActionContext.getRequest()));
		int pageunit = Integer.parseInt(this.getPageunit(ServletActionContext.getRequest(), "querypageunit"));

		String url = "menmian_queryMenmian?a=a";
		
		PageInfo pageInfo = this.menmianServices.queryMenmian(curpage,
				pageunit, ServletActionContext.getRequest(), url, cond.toString());
		if(getRequestParameter("flag")!=null&& "0".equals(getRequestParameter("flag"))){
			setSessionAttribute("mesg", "");
		}
		/*SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		String strTime = format.format(new Date());*/
		setRequestAttribute("pageinfo", pageInfo);
		setRequestAttribute("searchname", this.searchname);
		return "queryMenmian";
	}
	
	
	/**
	 * 
	 *
	 * 添加
	 *
	 */
	public String addMenmian(){
		try {
			TMenmian tmenmian = new TMenmian();
			tmenmian.setCourseName(courseName);
			tmenmian.setDizi(dizi);
			tmenmian.setMianji(mianji);
			tmenmian.setRemark(remark);
			tmenmian.setTel(tel);
			tmenmian.setUsername(username);
			tmenmian.setZujin(zujin);
			menmianServices.addMenmian(tmenmian);
		} catch (RuntimeException e) {
			e.printStackTrace();
		}
		return "addMenmian";
	}
	
	/**
	 * 
	 *
	 * 预修改
	 *
	 */
	public String preupdateMenmian() throws Exception{
		TMenmian menmian = this.getMenmianServices().getMenmian(Integer.parseInt(id));
		setRequestAttribute("menmian",menmian);
		return "preupdateMenmian";
	}
	
	/**
	 * 
	 *
	 * 功能：修改
	 *
	 */
	public String updateMenmian() {
		try {
			TMenmian tmenmian = this.menmianServices.getMenmian(Integer.parseInt(id));
			tmenmian.setCourseName(courseName);
			tmenmian.setDizi(dizi);
			tmenmian.setMianji(mianji);
			tmenmian.setRemark(remark);
			tmenmian.setTel(tel);
			tmenmian.setUsername(username);
			tmenmian.setZujin(zujin);
			this.getMenmianServices().updateMenmian(tmenmian);
		} catch (RuntimeException e) {
			e.printStackTrace();
		}
		return "updateMenmian";
	}
	/**
	 * 
	 *
	 * 删除
	 *
	 */
	public String delMenmian() throws Exception{
		menmianServices.delMenmian(Integer.parseInt(id));
		return "delMenmian";
	}

	public MenmianServices getMenmianServices() {
		return menmianServices;
	}

	public void setMenmianServices(MenmianServices menmianServices) {
		this.menmianServices = menmianServices;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getDizi() {
		return dizi;
	}

	public void setDizi(String dizi) {
		this.dizi = dizi;
	}

	public String getZujin() {
		return zujin;
	}

	public void setZujin(String zujin) {
		this.zujin = zujin;
	}

	public String getMianji() {
		return mianji;
	}

	public void setMianji(String mianji) {
		this.mianji = mianji;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getSearchname() {
		return searchname;
	}

	public void setSearchname(String searchname) {
		this.searchname = searchname;
	}


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}
	
	
}
