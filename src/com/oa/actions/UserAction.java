package com.oa.actions;
import org.apache.struts2.ServletActionContext;

import com.oa.models.TUser;
import com.oa.services.UserServices;
import com.oa.utils.PageInfo;

/**
 * 
 * 
 * 用户
 *
 */
public class UserAction extends BaseAction{
	private UserServices userServices;
	private Integer id;
	private String username;
	private String password;
	private String realname;
	private String sex;
	private String tel;
	private String className;
	private String major;
	private String email;
	private String qq;
	private String power;
	private String searchname;
	/**
	 * 
	 *管理员
	 * 分页
	 *
	 */
	public String queryUser() throws Exception{
		if (getSessionAttribute("querypageunit") == null) {
			setSessionAttribute("querypageunit",this.pageunit);
		}
		StringBuffer cond = new StringBuffer();
		if(null!=searchname&&""!=searchname.trim()){
			cond.append(" and a.username like '%"+searchname.trim()+"%' ");
		}
		cond.append(" and a.power =0 ");
		int curpage = Integer.parseInt(this.getCurrentpage(ServletActionContext.getRequest()));
		int pageunit = Integer.parseInt(this.getPageunit(ServletActionContext.getRequest(), "querypageunit"));

		String url = "user_queryUser?a=a";
		
		PageInfo pageInfo = this.userServices.queryUser(curpage,
				pageunit, ServletActionContext.getRequest(), url, cond.toString());
		setRequestAttribute("pageinfo", pageInfo);
		setRequestAttribute("searchname", this.searchname);
		return "queryUser";
	}
	
	
	/**
	 * 住户
	 * queryZhuhu:(这里用一句话描述这个方法的作用)
	 * TODO(这里描述这个方法适用条件 – 可选)
	 * TODO(这里描述这个方法的执行流程 – 可选)
	 * TODO(这里描述这个方法的使用方法 – 可选)
	 * TODO(这里描述这个方法的注意事项 – 可选)
	 *
	 * @param  @return
	 * @param  @throws Exception    设定文件
	 * @return String    DOM对象
	 * @throws 
	 * @since  CodingExample　Ver 1.1
	 */
	public String queryZhuhu() throws Exception{
		if (getSessionAttribute("querypageunit") == null) {
			setSessionAttribute("querypageunit",this.pageunit);
		}
		StringBuffer cond = new StringBuffer();
		if(null!=searchname&&""!=searchname.trim()){
			cond.append(" and a.username like '%"+searchname.trim()+"%' ");
		}
		cond.append(" and a.power =1 ");
		int curpage = Integer.parseInt(this.getCurrentpage(ServletActionContext.getRequest()));
		int pageunit = Integer.parseInt(this.getPageunit(ServletActionContext.getRequest(), "querypageunit"));

		String url = "user_queryZhuhu?a=a";
		
		PageInfo pageInfo = this.userServices.queryUser(curpage,
				pageunit, ServletActionContext.getRequest(), url, cond.toString());
		setRequestAttribute("pageinfo", pageInfo);
		setRequestAttribute("searchname", this.searchname);
		return "queryZhuhu";
	}
	/**
	 * 
	 *
	 * 添加
	 *
	 */
	public String addUser() throws Exception{
		TUser user = new TUser();
		user.setPassword(password);
		user.setPower("0");
		user.setRealname(realname);
		user.setUsername(username);
		user.setClassName(className);
		user.setEmail(email);
		user.setMajor(major);
		user.setQq(qq);
		user.setSex(sex);
		user.setTel(tel);
		userServices.addUser(user);
		return "addUser";
	}
	/**
	 * 
	 *
	 * 添加
	 *
	 */
	public String addZhuhu() throws Exception{
		TUser user = new TUser();
		user.setPassword(password);
		user.setPower("1");
		user.setRealname(realname);
		user.setUsername(username);
		user.setClassName(className);
		user.setEmail(email);
		user.setMajor(major);
		user.setQq(qq);
		user.setSex(sex);
		user.setTel(tel);
		userServices.addUser(user);
		return "addZhuhu";
	}
	/**
	 * 
	 *
	 * 预修改
	 *
	 */
	public String preupdateUser() throws Exception{
		TUser user = userServices.getUser(id);
		setRequestAttribute("user",user);
		return "preupdateUser";
	}
	/**
	 * 
	 *
	 * 预修改
	 *
	 */
	public String preupdateZhuhu() throws Exception{
		TUser user = userServices.getUser(id);
		setRequestAttribute("user",user);
		return "preupdateZhuhu";
	}
	/**
	 * 
	 *
	 * 个人信息管理
	 *
	 */
	public String preupdate() throws Exception{
		TUser user = userServices.getUser(((TUser)getSessionAttribute("user")).getId());
		setRequestAttribute("user",user);
		return "preupdate";
	}
	public String show() throws Exception{
		TUser user = userServices.getUser(((TUser)getSessionAttribute("user")).getId());
		setRequestAttribute("user",user);
		return "show";
	}
	/**
	 * 
	 *
	 * 修改
	 *
	 */
	public String updateUser() throws Exception{
		TUser user = userServices.getUser(id);
		user.setPassword(password);
		user.setRealname(realname);
		user.setClassName(className);
		user.setEmail(email);
		user.setMajor(major);
		user.setQq(qq);
		user.setSex(sex);
		user.setTel(tel);
		userServices.updateUser(user);
		return "updateUser";
	}
	/**
	 * 
	 *
	 * 修改
	 *
	 */
	public String updateZhuhu() throws Exception{
		TUser user = userServices.getUser(id);
		user.setPassword(password);
		user.setRealname(realname);
		user.setClassName(className);
		user.setEmail(email);
		user.setMajor(major);
		user.setQq(qq);
		user.setSex(sex);
		user.setTel(tel);
		userServices.updateUser(user);
		return "updateZhuhu";
	}
	
	/**
	 * 
	 *
	 * 修改
	 *
	 */
	public String updateMy() throws Exception{
		TUser user = userServices.getUser(id);
		user.setPassword(password);
		user.setRealname(realname);
		user.setClassName(className);
		user.setEmail(email);
		user.setMajor(major);
		user.setQq(qq);
		user.setSex(sex);
		user.setTel(tel);
		userServices.updateUser(user);
		return "updateMy";
	}
	/**
	 * 
	 *
	 * 删除
	 *
	 */
	public String delUser() throws Exception{
		TUser user = userServices.getUser(id);
		if(!"admin".equals(user.getUsername())){
			userServices.delUser(id);
		}
		return "delUser";
	}
	
	/**
	 * 
	 *
	 * 删除
	 *
	 */
	public String delZhuhu() throws Exception{
		userServices.delUser(id);
		return "delZhuhu";
	}
	public UserServices getUserServices() {
		return userServices;
	}
	public void setUserServices(UserServices userServices) {
		this.userServices = userServices;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRealname() {
		return realname;
	}
	public void setRealname(String realname) {
		this.realname = realname;
	}
	public String getPower() {
		return power;
	}
	public void setPower(String power) {
		this.power = power;
	}
	public String getSearchname() {
		return searchname;
	}
	public void setSearchname(String searchname) {
		this.searchname = searchname;
	}


	public String getSex() {
		return sex;
	}


	public void setSex(String sex) {
		this.sex = sex;
	}


	public String getTel() {
		return tel;
	}


	public void setTel(String tel) {
		this.tel = tel;
	}


	public String getClassName() {
		return className;
	}


	public void setClassName(String className) {
		this.className = className;
	}


	public String getMajor() {
		return major;
	}


	public void setMajor(String major) {
		this.major = major;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getQq() {
		return qq;
	}


	public void setQq(String qq) {
		this.qq = qq;
	}
	
}
