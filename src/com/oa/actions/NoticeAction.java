package com.oa.actions;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.struts2.ServletActionContext;

import com.oa.models.TNotice;
import com.oa.services.NoticeServices;
import com.oa.utils.PageInfo;

/**
 * 
 * 
 * 公告
 *
 */
public class NoticeAction extends BaseAction{
	private NoticeServices noticeServices;
	private Integer id;
	private String title;
	private String searchname;
	private String remark;
	/**
	 * 
	 *
	 * 分页
	 *
	 */
	public String queryNotice() throws Exception{
		if (getSessionAttribute("querypageunit") == null) {
			setSessionAttribute("querypageunit",this.pageunit);
		}
		StringBuffer cond = new StringBuffer();
		if(null!=searchname&&""!=searchname.trim()){
			cond.append(" and a.title like '%"+searchname.trim()+"%' ");
		}
		int curpage = Integer.parseInt(this.getCurrentpage(ServletActionContext.getRequest()));
		int pageunit = Integer.parseInt(this.getPageunit(ServletActionContext.getRequest(), "querypageunit"));

		String url = "notice_queryNotice?a=a";
		
		PageInfo pageInfo = this.noticeServices.queryNotice(curpage,
				pageunit, ServletActionContext.getRequest(), url, cond.toString());
		setRequestAttribute("pageinfo", pageInfo);
		setRequestAttribute("searchname", this.searchname);
		return "queryNotice";
	}
	/**
	 * 
	 *
	 * 添加
	 *
	 */
	public String addNotice(){
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String strTime = format.format(new Date());
			TNotice tnotice = new TNotice();
			tnotice.setTitle(title);
			tnotice.setRemark(remark);
			tnotice.setCreateTime(strTime);
			noticeServices.addNotice(tnotice);
		} catch (RuntimeException e) {
			e.printStackTrace();
		}
		return "addNotice";
	}
	/**
	 * 
	 *
	 * 预添加
	 *
	 */
	public String preaddNotice() throws Exception{
		return "preaddNotice";
	}
	/**
	 * 
	 *
	 * 预修改
	 *
	 */
	public String preupdateNotice() throws Exception{
		TNotice notice = this.getNoticeServices().getNotice(id);
		setRequestAttribute("notice",notice);
		return "preupdateNotice";
	}
	 public String showNotice() throws Exception{
			TNotice notice = this.getNoticeServices().getNotice(id);
			setRequestAttribute("notice",notice);
			return "showNotice";
	}
	/**
	 * 
	 *
	 * 功能：修改
	 *
	 */
	public String updateNotice() {
		try {
			TNotice tnotice = this.getNoticeServices().getNotice(id);
			tnotice.setTitle(title);
			tnotice.setRemark(remark);
			this.getNoticeServices().updateNotice(tnotice);
		} catch (RuntimeException e) {
			e.printStackTrace();
		}
		return "updateNotice";
	}
	/**
	 * 
	 *
	 * 删除
	 *
	 */
	public String delNotice() throws Exception{
		noticeServices.delNotice(id);
		return "delNotice";
	}
	public NoticeServices getNoticeServices() {
		return noticeServices;
	}
	public void setNoticeServices(NoticeServices noticeServices) {
		this.noticeServices = noticeServices;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getSearchname() {
		return searchname;
	}
	public void setSearchname(String searchname) {
		this.searchname = searchname;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
}
