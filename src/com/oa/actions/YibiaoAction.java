package com.oa.actions;
import org.apache.struts2.ServletActionContext;

import com.oa.models.TYibiao;
import com.oa.services.YibiaoServices;
import com.oa.utils.PageInfo;

/**
 * 
 */
public class YibiaoAction extends BaseAction{
	private YibiaoServices yibiaoServices;
	private Integer id;
	private String zhuhu;
	private String bianhao;
	private String startTime;
	private String endTime;
	private String feiyong;
	private String shuzhi;
	private String searchname;
	/**
	 * 
	 *
	 * 分页
	 *
	 */
	public String queryYibiao() throws Exception{
		if (getSessionAttribute("querypageunit") == null) {
			setSessionAttribute("querypageunit",this.pageunit);
		}
		StringBuffer cond = new StringBuffer();
		int curpage = Integer.parseInt(this.getCurrentpage(ServletActionContext.getRequest()));
		int pageunit = Integer.parseInt(this.getPageunit(ServletActionContext.getRequest(), "querypageunit"));

		String url = "yibiao_queryYibiao?a=a";
		if(null!=searchname&&""!=searchname.trim()){
			cond.append(" and a.bianhao like '%"+searchname.trim()+"%' ");
		}
		PageInfo pageInfo = this.yibiaoServices.queryYibiao(curpage,
				pageunit, ServletActionContext.getRequest(), url, cond.toString());
		setRequestAttribute("pageinfo", pageInfo);
		return "queryYibiao";
	}
	/**
	 * 
	 *
	 * 添加
	 *
	 */
	public String addYibiao(){
		try {
			/*SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String strTime = format.format(new Date());*/
			TYibiao tyibiao = new TYibiao();
			tyibiao.setBianhao(bianhao);
			tyibiao.setShuzhi(shuzhi);
			tyibiao.setEndTime(endTime);
			tyibiao.setFeiyong(feiyong);
			tyibiao.setStartTime(startTime);
			tyibiao.setZhuhu(zhuhu);
			yibiaoServices.addYibiao(tyibiao);
		} catch (RuntimeException e) {
			e.printStackTrace();
		}
		return "addYibiao";
	}
	/**
	 * 
	 *
	 * 预添加
	 *
	 */
	public String preupdateYibiao() throws Exception{
		TYibiao yibiao =  yibiaoServices.getYibiao(id);
		setRequestAttribute("yibiao", yibiao);
		return "preupdateYibiao";
	}
	/**
	 * 
	 *
	 * 功能：修改
	 *
	 */
	public String updateYibiao() {
		try {
			TYibiao tyibiao =  yibiaoServices.getYibiao(id);
			tyibiao.setBianhao(bianhao);
			tyibiao.setShuzhi(shuzhi);
			tyibiao.setEndTime(endTime);
			tyibiao.setFeiyong(feiyong);
			tyibiao.setStartTime(startTime);
			tyibiao.setZhuhu(zhuhu);
			yibiaoServices.updateYibiao(tyibiao);
		} catch (RuntimeException e) {
			e.printStackTrace();
		}
		return "updateYibiao";
	}
	/**
	 * 
	 *
	 * 删除
	 *
	 */
	public String delYibiao() throws Exception{
		yibiaoServices.delYibiao(id);
		return "delYibiao";
	}
	public YibiaoServices getYibiaoServices() {
		return yibiaoServices;
	}
	public void setYibiaoServices(YibiaoServices yibiaoServices) {
		this.yibiaoServices = yibiaoServices;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getZhuhu() {
		return zhuhu;
	}
	public void setZhuhu(String zhuhu) {
		this.zhuhu = zhuhu;
	}
	public String getBianhao() {
		return bianhao;
	}
	public void setBianhao(String bianhao) {
		this.bianhao = bianhao;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public String getFeiyong() {
		return feiyong;
	}
	public void setFeiyong(String feiyong) {
		this.feiyong = feiyong;
	}
	public String getShuzhi() {
		return shuzhi;
	}
	public void setShuzhi(String shuzhi) {
		this.shuzhi = shuzhi;
	}
	public String getSearchname() {
		return searchname;
	}
	public void setSearchname(String searchname) {
		this.searchname = searchname;
	}
}
