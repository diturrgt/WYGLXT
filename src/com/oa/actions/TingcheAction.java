package com.oa.actions;
import org.apache.struts2.ServletActionContext;

import com.oa.models.TTingche;
import com.oa.services.TingcheServices;
import com.oa.utils.PageInfo;

/**
 * 
 */
public class TingcheAction extends BaseAction{
	private TingcheServices tingcheServices;
	private Integer id;
	private String chepai;
	private String bianhao;
	private String startTime;
	private String endTime;
	private String feiyong;
	private String searchname;
	/**
	 * 
	 *
	 * 分页
	 *
	 */
	public String queryTingche() throws Exception{
		if (getSessionAttribute("querypageunit") == null) {
			setSessionAttribute("querypageunit",this.pageunit);
		}
		StringBuffer cond = new StringBuffer();
		int curpage = Integer.parseInt(this.getCurrentpage(ServletActionContext.getRequest()));
		int pageunit = Integer.parseInt(this.getPageunit(ServletActionContext.getRequest(), "querypageunit"));

		String url = "tingche_queryTingche?a=a";
		if(null!=searchname&&""!=searchname.trim()){
			cond.append(" and a.chepai like '%"+searchname.trim()+"%' ");
		}
		PageInfo pageInfo = this.tingcheServices.queryTingche(curpage,
				pageunit, ServletActionContext.getRequest(), url, cond.toString());
		setRequestAttribute("pageinfo", pageInfo);
		return "queryTingche";
	}
	/**
	 * 
	 *
	 * 添加
	 *
	 */
	public String addTingche(){
		try {
			/*SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String strTime = format.format(new Date());*/
			TTingche ttingche = new TTingche();
			ttingche.setBianhao(bianhao);
			ttingche.setChepai(chepai);
			ttingche.setEndTime(endTime);
			ttingche.setFeiyong(feiyong);
			ttingche.setStartTime(startTime);
			tingcheServices.addTingche(ttingche);
		} catch (RuntimeException e) {
			e.printStackTrace();
		}
		return "addTingche";
	}
	/**
	 * 
	 *
	 * 预添加
	 *
	 */
	public String preupdateTingche() throws Exception{
		TTingche tingche =  tingcheServices.getTingche(id);
		setRequestAttribute("tingche", tingche);
		return "preupdateTingche";
	}
	/**
	 * 
	 *
	 * 功能：修改
	 *
	 */
	public String updateTingche() {
		try {
			TTingche ttingche =  tingcheServices.getTingche(id);
			ttingche.setBianhao(bianhao);
			ttingche.setChepai(chepai);
			ttingche.setEndTime(endTime);
			ttingche.setFeiyong(feiyong);
			ttingche.setStartTime(startTime);
			tingcheServices.updateTingche(ttingche);
		} catch (RuntimeException e) {
			e.printStackTrace();
		}
		return "updateTingche";
	}
	/**
	 * 
	 *
	 * 删除
	 *
	 */
	public String delTingche() throws Exception{
		tingcheServices.delTingche(id);
		return "delTingche";
	}
	public TingcheServices getTingcheServices() {
		return tingcheServices;
	}
	public void setTingcheServices(TingcheServices tingcheServices) {
		this.tingcheServices = tingcheServices;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getChepai() {
		return chepai;
	}
	public void setChepai(String chepai) {
		this.chepai = chepai;
	}
	public String getBianhao() {
		return bianhao;
	}
	public void setBianhao(String bianhao) {
		this.bianhao = bianhao;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public String getFeiyong() {
		return feiyong;
	}
	public void setFeiyong(String feiyong) {
		this.feiyong = feiyong;
	}
	public String getSearchname() {
		return searchname;
	}
	public void setSearchname(String searchname) {
		this.searchname = searchname;
	}
	
}
