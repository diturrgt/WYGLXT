package com.oa.actions;
import org.apache.struts2.ServletActionContext;

import com.oa.models.TBaoxiu;
import com.oa.models.TUser;
import com.oa.services.BaoxiuServices;
import com.oa.utils.PageInfo;

/**
 * 
 */
public class BaoxiuAction extends BaseAction{
	private BaoxiuServices baoxiuServices;
	private Integer id;
	private String username;
	private String title;
	private String remark;
	private String tel;
	private String dizi;
	private String searchname;
	/**
	 * 
	 *
	 * 分页
	 *
	 */
	public String queryBaoxiu() throws Exception{
		if (getSessionAttribute("querypageunit") == null) {
			setSessionAttribute("querypageunit",this.pageunit);
		}
		StringBuffer cond = new StringBuffer();
		int curpage = Integer.parseInt(this.getCurrentpage(ServletActionContext.getRequest()));
		int pageunit = Integer.parseInt(this.getPageunit(ServletActionContext.getRequest(), "querypageunit"));

		String url = "baoxiu_queryBaoxiu?a=a";
		if(null!=searchname&&""!=searchname.trim()){
			cond.append(" and a.title like '%"+searchname.trim()+"%' ");
		}
		if(null!=getSessionAttribute("user")&&"1".equals(((TUser)getSessionAttribute("user")).getPower())){
			cond.append(" and a.username = '"+((TUser)getSessionAttribute("user")).getRealname()+"' ");
		}
		PageInfo pageInfo = this.baoxiuServices.queryBaoxiu(curpage,
				pageunit, ServletActionContext.getRequest(), url, cond.toString());
		setRequestAttribute("pageinfo", pageInfo);
		return "queryBaoxiu";
	}
	/**
	 * 
	 *
	 * 添加
	 *
	 */
	public String addBaoxiu(){
		try {
			/*SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String strTime = format.format(new Date());*/
			TBaoxiu tbaoxiu = new TBaoxiu();
			tbaoxiu.setDizi(dizi);
			tbaoxiu.setRemark(remark);
			tbaoxiu.setTel(tel);
			tbaoxiu.setTitle(title);
			tbaoxiu.setUsername(((TUser)getSessionAttribute("user")).getRealname());
			baoxiuServices.addBaoxiu(tbaoxiu);
		} catch (RuntimeException e) {
			e.printStackTrace();
		}
		return "addBaoxiu";
	}
	/**
	 * 
	 *
	 * 预添加
	 *
	 */
	public String preupdateBaoxiu() throws Exception{
		TBaoxiu baoxiu =  baoxiuServices.getBaoxiu(id);
		setRequestAttribute("baoxiu", baoxiu);
		return "preupdateBaoxiu";
	}
	/**
	 * 
	 *
	 * 功能：修改
	 *
	 */
	public String updateBaoxiu() {
		try {
			TBaoxiu tbaoxiu =  baoxiuServices.getBaoxiu(id);
			tbaoxiu.setDizi(dizi);
			tbaoxiu.setRemark(remark);
			tbaoxiu.setTel(tel);
			tbaoxiu.setTitle(title);
			baoxiuServices.updateBaoxiu(tbaoxiu);
		} catch (RuntimeException e) {
			e.printStackTrace();
		}
		return "updateBaoxiu";
	}
	/**
	 * 
	 *
	 * 删除
	 *
	 */
	public String delBaoxiu() throws Exception{
		baoxiuServices.delBaoxiu(id);
		return "delBaoxiu";
	}
	public BaoxiuServices getBaoxiuServices() {
		return baoxiuServices;
	}
	public void setBaoxiuServices(BaoxiuServices baoxiuServices) {
		this.baoxiuServices = baoxiuServices;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getDizi() {
		return dizi;
	}
	public void setDizi(String dizi) {
		this.dizi = dizi;
	}
	public String getSearchname() {
		return searchname;
	}
	public void setSearchname(String searchname) {
		this.searchname = searchname;
	}
	
	
}
