package com.oa.models;

/**
 * TTousu entity.
 * 
 * @author MyEclipse Persistence Tools
 */

public class TTousu implements java.io.Serializable {
	private Integer id;
	private String num;
	private String remark;
	private String sizenum;
	public TTousu (Integer id){
		this.id = id;
	}
	
	public TTousu (){
		
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNum() {
		return num;
	}
	public void setNum(String num) {
		this.num = num;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getSizenum() {
		return sizenum;
	}
	public void setSizenum(String sizenum) {
		this.sizenum = sizenum;
	}
	
}