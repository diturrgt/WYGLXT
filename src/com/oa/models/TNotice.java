package com.oa.models;

import java.util.Date;

/**
 * TNotice entity.
 * 
 * @author MyEclipse Persistence Tools
 */

public class TNotice implements java.io.Serializable {

	// Fields

	private Integer id;
	private String title;
	private String remark;
	private String createTime;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
}