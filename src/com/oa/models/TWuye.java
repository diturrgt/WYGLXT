package com.oa.models;


/**
 * TNotice entity.
 * 
 * @author MyEclipse Persistence Tools
 */

public class TWuye implements java.io.Serializable {

	// Fields

	private Integer id;
	private String zhuhu;
	private String bianhao;
	private String startTime;
	private String endTime;
	private String feiyong;
	private String title;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getZhuhu() {
		return zhuhu;
	}
	public void setZhuhu(String zhuhu) {
		this.zhuhu = zhuhu;
	}
	public String getBianhao() {
		return bianhao;
	}
	public void setBianhao(String bianhao) {
		this.bianhao = bianhao;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public String getFeiyong() {
		return feiyong;
	}
	public void setFeiyong(String feiyong) {
		this.feiyong = feiyong;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
}