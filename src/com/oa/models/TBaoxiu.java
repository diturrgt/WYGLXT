package com.oa.models;

/**
 * TTousu entity.
 * 
 * @author MyEclipse Persistence Tools
 */

public class TBaoxiu implements java.io.Serializable {
	private Integer id;
	private String username;
	private String title;
	private String remark;
	private String tel;
	private String dizi;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getDizi() {
		return dizi;
	}
	public void setDizi(String dizi) {
		this.dizi = dizi;
	}
	
}