package com.oa.models;

/**
 * TTousu entity.
 * 
 * @author MyEclipse Persistence Tools
 */

public class TMenmian implements java.io.Serializable {
	private Integer id;
	private String courseName;
	private String remark;
	private String dizi;
	private String zujin;
	private String mianji;
	private String tel;
	private String username;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCourseName() {
		return courseName;
	}
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getDizi() {
		return dizi;
	}
	public void setDizi(String dizi) {
		this.dizi = dizi;
	}
	public String getZujin() {
		return zujin;
	}
	public void setZujin(String zujin) {
		this.zujin = zujin;
	}
	public String getMianji() {
		return mianji;
	}
	public void setMianji(String mianji) {
		this.mianji = mianji;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
}