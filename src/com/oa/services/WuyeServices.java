package com.oa.services;

import javax.servlet.http.HttpServletRequest;

import com.oa.models.TWuye;
import com.oa.utils.PageInfo;




public interface WuyeServices {
	//分页
	public PageInfo queryWuye(int currentpage, int pageunit,
			HttpServletRequest request, String url, String cond);
	//添加
	public Boolean addWuye(TWuye message);
	//修改
	public Boolean updateWuye(TWuye message);
	//查询
	public TWuye getWuye(Integer id);
	//删除
	public TWuye delWuye(Integer id);
}
