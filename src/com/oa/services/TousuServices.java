package com.oa.services;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.oa.models.TTousu;
import com.oa.utils.PageInfo;




public interface TousuServices {
	//分页
	public PageInfo queryTousu(int currentpage, int pageunit,
			HttpServletRequest request, String url, String cond);
	//添加
	public Boolean addTousu(TTousu tousu);
	//修改
	public Boolean updateTousu(TTousu tousu);
	//查询
	public TTousu getTousu(Integer id);
	//删除
	public TTousu delTousu(Integer id);
	
	public List<TTousu> queryTousu();
}
