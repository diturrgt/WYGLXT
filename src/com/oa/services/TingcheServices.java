package com.oa.services;

import javax.servlet.http.HttpServletRequest;

import com.oa.models.TTingche;
import com.oa.utils.PageInfo;




public interface TingcheServices {
	//分页
	public PageInfo queryTingche(int currentpage, int pageunit,
			HttpServletRequest request, String url, String cond);
	//添加
	public Boolean addTingche(TTingche message);
	//修改
	public Boolean updateTingche(TTingche message);
	//查询
	public TTingche getTingche(Integer id);
	//删除
	public TTingche delTingche(Integer id);
}
