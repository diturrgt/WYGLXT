package com.oa.services;

import javax.servlet.http.HttpServletRequest;

import com.oa.models.TMenmian;
import com.oa.utils.PageInfo;




public interface MenmianServices {
	//分页
	public PageInfo queryMenmian(int currentpage, int pageunit,
			HttpServletRequest request, String url, String cond);
	//添加
	public Boolean addMenmian(TMenmian menmian);
	//修改
	public Boolean updateMenmian(TMenmian menmian);
	//查询
	public TMenmian getMenmian(Integer id);
	//删除
	public TMenmian delMenmian(Integer id);
}
