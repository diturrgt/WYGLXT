package com.oa.services;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.oa.models.TUser;
import com.oa.utils.PageInfo;




public interface UserServices {
	//分页
	public PageInfo queryUser(int currentpage, int pageunit,
			HttpServletRequest request, String url, String cond);
	//添加
	public Boolean addUser(TUser supplier);
	//修改
	public Boolean updateUser(TUser supplier);
	//查询
	public TUser getUser(Integer id);
	//删除
	public TUser delUser(Integer id);
	
	public List<TUser> queryUser();
	public List<TUser> queryUser(String power);
}
