package com.oa.services;

import javax.servlet.http.HttpServletRequest;

import com.oa.models.TNotice;
import com.oa.utils.PageInfo;




public interface NoticeServices {
	//分页
	public PageInfo queryNotice(int currentpage, int pageunit,
			HttpServletRequest request, String url, String cond);
	//添加
	public Boolean addNotice(TNotice notice);
	//修改
	public Boolean updateNotice(TNotice notice);
	//查询
	public TNotice getNotice(Integer id);
	//删除
	public TNotice delNotice(Integer id);
}
