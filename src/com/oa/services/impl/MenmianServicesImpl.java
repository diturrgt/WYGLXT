package com.oa.services.impl;


import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.oa.models.TMenmian;
import com.oa.services.MenmianServices;
import com.oa.utils.PageInfo;
import com.oa.web.AbstractServices;


public class MenmianServicesImpl extends AbstractServices implements MenmianServices {
	/**
	 * 分页
	 */
	public PageInfo queryMenmian(int currentpage, int pageunit,
			HttpServletRequest request, String url, String cond) {
		int rowCount = this.getMenmiansCount(cond);
		List<TMenmian> list = this.getMenmians(currentpage,
				pageunit, cond);
		PageInfo PageInfo = new PageInfo(currentpage, pageunit, rowCount, url,
				list);
		return PageInfo;
	}
	
	/**
	 * 
	 *
	 * 分页
	 *
	 */
	public int getMenmiansCount(String cond) {
		try {
			String hql = " select count(a) from TMenmian a where 1=1 "
					+ cond;
			Object t = this.queryAggregation(hql);
			return ((Long) t).intValue();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
	/**
	 * 
	 *
	 * 查询
	 *
	 */
	public List<TMenmian> getMenmians(int currentpage, int pageunit,
			String cond) {

		try {
			String hql = " from TMenmian a where 1=1  " + cond;
			return this.query(hql, currentpage, pageunit);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * 添加
	 */
	public Boolean addMenmian(TMenmian menmian) {
		save(menmian);
		return true;
	}
	/**
	 * 查询
	 */
	public TMenmian getMenmian(Integer id) {
		
		return (TMenmian)load(TMenmian.class, id);
	}
	/**
	 * 删除
	 */
	public TMenmian delMenmian(Integer id) {
		deleteById(TMenmian.class, id);
		return null;
	}
	/**
	 * 修改
	 */
	public Boolean updateMenmian(TMenmian menmian) {
		update(menmian);
		return null;
	}

}
