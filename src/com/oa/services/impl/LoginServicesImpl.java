package com.oa.services.impl;

import java.util.List;
import com.oa.models.TUser;
import com.oa.services.LoginServices;
import com.oa.web.AbstractServices;



public class LoginServicesImpl extends AbstractServices implements LoginServices {
	
	public TUser checkUser(String username, String password, String power) {

		String wherejpql = "from TUser o where  o.username =? and o.password = ? and o.power = ?";

		List<TUser> list = getResultList(wherejpql,new Object[] {username, password, power});

		if (list.size() == 1) {
			return (TUser)list.get(0);
		}
		return null;
	}

	public Boolean updateUser(TUser user) {
		update(user);
		return true;
	}

}
