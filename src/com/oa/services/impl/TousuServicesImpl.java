package com.oa.services.impl;


import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.oa.models.TTousu;
import com.oa.models.TUser;
import com.oa.services.TousuServices;
import com.oa.utils.PageInfo;
import com.oa.web.AbstractServices;



public class TousuServicesImpl extends AbstractServices implements TousuServices {
	/**
	 * 分页
	 */
	public PageInfo queryTousu(int currentpage, int pageunit,
			HttpServletRequest request, String url, String cond) {
		int rowCount = this.getToususCount(cond);
		List<TTousu> list = this.getTousus(currentpage,
				pageunit, cond);
		PageInfo PageInfo = new PageInfo(currentpage, pageunit, rowCount, url,
				list);
		return PageInfo;
	}
	
	/**
	 * 
	 *
	 * 分页
	 *
	 */
	public int getToususCount(String cond) {
		try {
			String hql = " select count(a) from TTousu a where 1=1 "
					+ cond;
			Object t = this.queryAggregation(hql);
			return ((Long) t).intValue();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
	/**
	 * 
	 *
	 * 查询
	 *
	 */
	public List<TTousu> getTousus(int currentpage, int pageunit,
			String cond) {

		try {
			String hql = " from TTousu a where 1=1  " + cond;
			return this.query(hql, currentpage, pageunit);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * 添加
	 */
	public Boolean addTousu(TTousu message) {
		save(message);
		return true;
	}
	/**
	 * 查询
	 */
	public TTousu getTousu(Integer id) {
		
		return (TTousu)load(TTousu.class, id);
	}
	/**
	 * 删除
	 */
	public TTousu delTousu(Integer id) {
		deleteById(TTousu.class, id);
		return null;
	}
	/**
	 * 修改
	 */
	public Boolean updateTousu(TTousu message) {
		update(message);
		return null;
	}
	
	public List<TTousu> queryTousu() {
		String hql = " from TTousu a where 1=1 ";
		return getResultList(hql);
	}

}
