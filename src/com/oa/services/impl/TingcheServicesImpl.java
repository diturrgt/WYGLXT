package com.oa.services.impl;


import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.oa.models.TTingche;
import com.oa.services.TingcheServices;
import com.oa.utils.PageInfo;
import com.oa.web.AbstractServices;



public class TingcheServicesImpl extends AbstractServices implements TingcheServices {
	/**
	 * 分页
	 */
	public PageInfo queryTingche(int currentpage, int pageunit,
			HttpServletRequest request, String url, String cond) {
		int rowCount = this.getTingchesCount(cond);
		List<TTingche> list = this.getTingches(currentpage,
				pageunit, cond);
		PageInfo PageInfo = new PageInfo(currentpage, pageunit, rowCount, url,
				list);
		return PageInfo;
	}
	
	/**
	 * 
	 *
	 * 分页
	 *
	 */
	public int getTingchesCount(String cond) {
		try {
			String hql = " select count(a) from TTingche a where 1=1 "
					+ cond;
			Object t = this.queryAggregation(hql);
			return ((Long) t).intValue();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
	/**
	 * 
	 *
	 * 查询
	 *
	 */
	public List<TTingche> getTingches(int currentpage, int pageunit,
			String cond) {

		try {
			String hql = " from TTingche a where 1=1  " + cond;
			return this.query(hql, currentpage, pageunit);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * 添加
	 */
	public Boolean addTingche(TTingche message) {
		save(message);
		return true;
	}
	/**
	 * 查询
	 */
	public TTingche getTingche(Integer id) {
		
		return (TTingche)load(TTingche.class, id);
	}
	/**
	 * 删除
	 */
	public TTingche delTingche(Integer id) {
		deleteById(TTingche.class, id);
		return null;
	}
	/**
	 * 修改
	 */
	public Boolean updateTingche(TTingche message) {
		update(message);
		return null;
	}

}
