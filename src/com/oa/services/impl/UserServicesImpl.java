package com.oa.services.impl;


import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.oa.models.TUser;
import com.oa.services.UserServices;
import com.oa.utils.PageInfo;
import com.oa.web.AbstractServices;



public class UserServicesImpl extends AbstractServices implements UserServices {
	/**
	 * 分页
	 */
	public PageInfo queryUser(int currentpage, int pageunit,
			HttpServletRequest request, String url, String cond) {
		int rowCount = this.getUsersCount(cond);
		List<TUser> list = this.getUsers(currentpage,
				pageunit, cond);
		PageInfo PageInfo = new PageInfo(currentpage, pageunit, rowCount, url,
				list);
		return PageInfo;
	}
	
	/**
	 * 
	 *
	 * 分页
	 *
	 */
	public int getUsersCount(String cond) {
		try {
			String hql = " select count(a) from TUser a where 1=1 "
					+ cond;
			Object t = this.queryAggregation(hql);
			return ((Long) t).intValue();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
	/**
	 * 
	 *
	 * 查询
	 *
	 */
	public List<TUser> getUsers(int currentpage, int pageunit,
			String cond) {

		try {
			String hql = " from TUser a where 1=1  " + cond;
			return this.query(hql, currentpage, pageunit);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * 添加
	 */
	public Boolean addUser(TUser supplier) {
		save(supplier);
		return true;
	}
	/**
	 * 查询
	 */
	public TUser getUser(Integer id) {
		
		return (TUser)load(TUser.class, id);
	}
	/**
	 * 删除
	 */
	public TUser delUser(Integer id) {
		deleteById(TUser.class, id);
		return null;
	}
	/**
	 * 修改
	 */
	public Boolean updateUser(TUser supplier) {
		update(supplier);
		return null;
	}

	public List<TUser> queryUser() {
		String hql = " from TUser a where 1=1  ";
		return getResultList(hql);
	}
	public List<TUser> queryUser(String power) {
		String hql = " from TUser a where 1=1 and a.power="+power;
		return getResultList(hql);
	}
}
