package com.oa.services.impl;


import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.oa.models.TWuye;
import com.oa.services.WuyeServices;
import com.oa.utils.PageInfo;
import com.oa.web.AbstractServices;



public class WuyeServicesImpl extends AbstractServices implements WuyeServices {
	/**
	 * 分页
	 */
	public PageInfo queryWuye(int currentpage, int pageunit,
			HttpServletRequest request, String url, String cond) {
		int rowCount = this.getWuyesCount(cond);
		List<TWuye> list = this.getWuyes(currentpage,
				pageunit, cond);
		PageInfo PageInfo = new PageInfo(currentpage, pageunit, rowCount, url,
				list);
		return PageInfo;
	}
	
	/**
	 * 
	 *
	 * 分页
	 *
	 */
	public int getWuyesCount(String cond) {
		try {
			String hql = " select count(a) from TWuye a where 1=1 "
					+ cond;
			Object t = this.queryAggregation(hql);
			return ((Long) t).intValue();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
	/**
	 * 
	 *
	 * 查询
	 *
	 */
	public List<TWuye> getWuyes(int currentpage, int pageunit,
			String cond) {

		try {
			String hql = " from TWuye a where 1=1  " + cond;
			return this.query(hql, currentpage, pageunit);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * 添加
	 */
	public Boolean addWuye(TWuye message) {
		save(message);
		return true;
	}
	/**
	 * 查询
	 */
	public TWuye getWuye(Integer id) {
		
		return (TWuye)load(TWuye.class, id);
	}
	/**
	 * 删除
	 */
	public TWuye delWuye(Integer id) {
		deleteById(TWuye.class, id);
		return null;
	}
	/**
	 * 修改
	 */
	public Boolean updateWuye(TWuye message) {
		update(message);
		return null;
	}

}
