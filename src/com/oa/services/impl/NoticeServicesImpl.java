package com.oa.services.impl;


import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.oa.models.TNotice;
import com.oa.services.NoticeServices;
import com.oa.utils.PageInfo;
import com.oa.web.AbstractServices;



public class NoticeServicesImpl extends AbstractServices implements NoticeServices {
	/**
	 * 分页
	 */
	public PageInfo queryNotice(int currentpage, int pageunit,
			HttpServletRequest request, String url, String cond) {
		int rowCount = this.getNoticesCount(cond);
		List<TNotice> list = this.getNotices(currentpage,
				pageunit, cond);
		PageInfo PageInfo = new PageInfo(currentpage, pageunit, rowCount, url,
				list);
		return PageInfo;
	}
	
	/**
	 * 
	 *
	 * 分页
	 *
	 */
	public int getNoticesCount(String cond) {
		try {
			String hql = " select count(a) from TNotice a where 1=1 "
					+ cond;
			Object t = this.queryAggregation(hql);
			return ((Long) t).intValue();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
	/**
	 * 
	 *
	 * 查询
	 *
	 */
	public List<TNotice> getNotices(int currentpage, int pageunit,
			String cond) {

		try {
			String hql = " from TNotice a where 1=1  " + cond;
			return this.query(hql, currentpage, pageunit);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * 添加
	 */
	public Boolean addNotice(TNotice notice) {
		save(notice);
		return true;
	}
	/**
	 * 查询
	 */
	public TNotice getNotice(Integer id) {
		
		return (TNotice)load(TNotice.class, id);
	}
	/**
	 * 删除
	 */
	public TNotice delNotice(Integer id) {
		deleteById(TNotice.class, id);
		return null;
	}
	/**
	 * 修改
	 */
	public Boolean updateNotice(TNotice notice) {
		update(notice);
		return null;
	}

}
