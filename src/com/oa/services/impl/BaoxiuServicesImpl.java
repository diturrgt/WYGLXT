package com.oa.services.impl;


import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.oa.models.TBaoxiu;
import com.oa.services.BaoxiuServices;
import com.oa.utils.PageInfo;
import com.oa.web.AbstractServices;



public class BaoxiuServicesImpl extends AbstractServices implements BaoxiuServices {
	/**
	 * 分页
	 */
	public PageInfo queryBaoxiu(int currentpage, int pageunit,
			HttpServletRequest request, String url, String cond) {
		int rowCount = this.getBaoxiusCount(cond);
		List<TBaoxiu> list = this.getBaoxius(currentpage,
				pageunit, cond);
		PageInfo PageInfo = new PageInfo(currentpage, pageunit, rowCount, url,
				list);
		return PageInfo;
	}
	
	/**
	 * 
	 *
	 * 分页
	 *
	 */
	public int getBaoxiusCount(String cond) {
		try {
			String hql = " select count(a) from TBaoxiu a where 1=1 "
					+ cond;
			Object t = this.queryAggregation(hql);
			return ((Long) t).intValue();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
	/**
	 * 
	 *
	 * 查询
	 *
	 */
	public List<TBaoxiu> getBaoxius(int currentpage, int pageunit,
			String cond) {

		try {
			String hql = " from TBaoxiu a where 1=1  " + cond;
			return this.query(hql, currentpage, pageunit);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * 添加
	 */
	public Boolean addBaoxiu(TBaoxiu message) {
		save(message);
		return true;
	}
	/**
	 * 查询
	 */
	public TBaoxiu getBaoxiu(Integer id) {
		
		return (TBaoxiu)load(TBaoxiu.class, id);
	}
	/**
	 * 删除
	 */
	public TBaoxiu delBaoxiu(Integer id) {
		deleteById(TBaoxiu.class, id);
		return null;
	}
	/**
	 * 修改
	 */
	public Boolean updateBaoxiu(TBaoxiu message) {
		update(message);
		return null;
	}

}
