package com.oa.services.impl;


import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.oa.models.TYibiao;
import com.oa.services.YibiaoServices;
import com.oa.utils.PageInfo;
import com.oa.web.AbstractServices;



public class YibaoServicesImpl extends AbstractServices implements YibiaoServices {
	/**
	 * 分页
	 */
	public PageInfo queryYibiao(int currentpage, int pageunit,
			HttpServletRequest request, String url, String cond) {
		int rowCount = this.getYibiaosCount(cond);
		List<TYibiao> list = this.getYibiaos(currentpage,
				pageunit, cond);
		PageInfo PageInfo = new PageInfo(currentpage, pageunit, rowCount, url,
				list);
		return PageInfo;
	}
	
	/**
	 * 
	 *
	 * 分页
	 *
	 */
	public int getYibiaosCount(String cond) {
		try {
			String hql = " select count(a) from TYibiao a where 1=1 "
					+ cond;
			Object t = this.queryAggregation(hql);
			return ((Long) t).intValue();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
	/**
	 * 
	 *
	 * 查询
	 *
	 */
	public List<TYibiao> getYibiaos(int currentpage, int pageunit,
			String cond) {

		try {
			String hql = " from TYibiao a where 1=1  " + cond;
			return this.query(hql, currentpage, pageunit);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * 添加
	 */
	public Boolean addYibiao(TYibiao message) {
		save(message);
		return true;
	}
	/**
	 * 查询
	 */
	public TYibiao getYibiao(Integer id) {
		
		return (TYibiao)load(TYibiao.class, id);
	}
	/**
	 * 删除
	 */
	public TYibiao delYibiao(Integer id) {
		deleteById(TYibiao.class, id);
		return null;
	}
	/**
	 * 修改
	 */
	public Boolean updateYibiao(TYibiao message) {
		update(message);
		return null;
	}

}
