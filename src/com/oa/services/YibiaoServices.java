package com.oa.services;

import javax.servlet.http.HttpServletRequest;

import com.oa.models.TYibiao;
import com.oa.utils.PageInfo;




public interface YibiaoServices {
	//分页
	public PageInfo queryYibiao(int currentpage, int pageunit,
			HttpServletRequest request, String url, String cond);
	//添加
	public Boolean addYibiao(TYibiao message);
	//修改
	public Boolean updateYibiao(TYibiao message);
	//查询
	public TYibiao getYibiao(Integer id);
	//删除
	public TYibiao delYibiao(Integer id);
}
