package com.oa.services;

import javax.servlet.http.HttpServletRequest;

import com.oa.models.TBaoxiu;
import com.oa.utils.PageInfo;




public interface BaoxiuServices {
	//分页
	public PageInfo queryBaoxiu(int currentpage, int pageunit,
			HttpServletRequest request, String url, String cond);
	//添加
	public Boolean addBaoxiu(TBaoxiu message);
	//修改
	public Boolean updateBaoxiu(TBaoxiu message);
	//查询
	public TBaoxiu getBaoxiu(Integer id);
	//删除
	public TBaoxiu delBaoxiu(Integer id);
}
