package com.oa.services;

import com.oa.models.TUser;



public interface LoginServices {
	public TUser checkUser(String username, String password,String power);
	public Boolean updateUser(TUser user);
}
